#!/bin/bash

### WARNING MSG ###
echo ""
echo "BE SURE PORTS 5672,15672,4369,25672 ARE OPEN"
echo ""
sleep 3
echo ""

### INSTALL PACKAGES ###
yum update
yum install epel-release
wget http://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm
rpm -Uvh erlang-solutions-1.0-1.noarch.rpm
yum update
yum install erlang
wget https://www.rabbitmq.com/releases/rabbitmq-server/v3.6.1/rabbitmq-server-3.6.1-1.noarch.rpm
rpm --import https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
yum install rabbitmq-server-3.6.1-1.noarch.rpm

### CLEAN ###
rm -rf erlang-solutions-1.0-1.noarch.rpm rabbitmq-server-3.6.1-1.noarch.rpm
yum clean all

### SETTINGS RABBITMQ ###
rabbitmq-plugins enable rabbitmq_management
service rabbitmq-server restart
add_user tango tango

### END MSG ###
echo ""
echo "DONE, YOU CAN NOW ACCESS CONFIGURATION INTERFACE WITH YOUR BROWSER:"
echo "http://localhost:15672/#/"
echo "(default user,password is guest:guest)"
