### build a new image
```
docker build -t <name>:<tag> .
```

### run a container without(1)/with(2) shared volume 
```
docker run --net=host -it <name>:<tag>
docker run --net=host -v <host_fullpath>:<container_fullpath> -it <name>:<tag>
```

### start/stop a container
```
docker <start|stop> <id>
```

### connect to container
```
docker exec -it <id> bash
```

### see docker containers list (-a to show non running containers)
```
docker ps [-a]
```

### list pre-builded images
```
docker images
```

### remove image (rmi=image rm=container)
```
docker rm[i] -f <id>
```

### connect as root
```
docker exec -u 0 -it <id> bash
```

### tar an image / a container
```
docker save <id> > save_img.tar
docker export <id> > save_cont.tar
```

### load image / container from tar
```
docker load < img.tar
cat container.tar | docker import - <name>:<tag>
```
