FROM centos:centos6
MAINTAINER D24

# yum update
    RUN yum -y update
    
# yum install essentials
    RUN yum -y install wget cvs gcc gcc-c++ curl perl-XML-Parser-2.36-7.el6.x86_64

# yum install tango_requirements
    RUN yum -y install libxml2-devel libxslt-devel openssl-devel expat-devel libcurl-devel mysql-devel

# yum clean
    RUN yum clean all

# open cvs ports
EXPOSE 5672 15672 4369 25672 
#    EXPOSE 12401 2401

# set timezone to Europe/Paris
    RUN mv /etc/localtime /etc/localtimeBAK
    RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime

# create user
    RUN useradd -ms /bin/bash tango

# create home
    WORKDIR /home/tango
    RUN mkdir -p gen test/dic test/config test/simu test/def_cfg

# copy files
    ADD files/init_env.sh .
    ADD files/versions gen/
    ADD files/product.lib gen/
    ADD files/sample_config.tar test/def_cfg
    ADD files/sample.xml test/simu/
    ADD files/bin.tar .

# open tango ports in /etc/services
    RUN echo "D24_HYPERCMD         17501/tcp" >> /etc/services
    RUN echo "D24_CENTRCMDCMD      17504/tcp" >> /etc/services
    RUN echo "D24_BATCHINCMD       17505/tcp" >> /etc/services
    RUN echo "D24_LOGPORT          17511/tcp" >> /etc/services
    RUN echo "D24_TRACEPORT        17512/tcp" >> /etc/services
    RUN echo "D24_STATPORT         17513/tcp" >> /etc/services
    RUN echo "D24_AUDITPORT        17514/tcp" >> /etc/services
    RUN echo "D24_CYPHERPORT       17515/tcp" >> /etc/services
    RUN echo "D24_CMDPORT          17516/tcp" >> /etc/services
    RUN echo "D24_GUIPORT          17520/tcp" >> /etc/services
    RUN echo "D24_BATCHINPORT      17521/tcp" >> /etc/services
    RUN echo "D24_CONSPORT         17522/tcp" >> /etc/services

# set bash_rc
    RUN echo "export CVSROOT=:pserver:d24@jupiter.lusis:12401/lusis" >> .bashrc
    RUN echo "export PRODUCT=/home/tango/gen" >> .bashrc
    RUN echo "export PATH=.:./bin:../bin/:/home/tango/bin:./command:/home/tango/command:/usr/lib64/qt-3.3/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/tango/bin:.::/home/tango/valgrind/bin" >> .bashrc
    RUN echo "export TANGOENV=D24" >> .bashrc
    RUN echo "export LOGNAME=tango" >> .bashrc
    RUN echo "export QTLIB=/usr/lib64/qt-3.3/lib" >> .bashrc
    RUN echo "export LD_LIBRARY_PATH=./lib:./module:" >> .bashrc

# set home rights
    WORKDIR /home/tango
    RUN chown -R tango .

# now set user
    USER tango
