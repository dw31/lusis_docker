#!/bin/bash
echo ""
echo "--------------------------------------"
echo "This script will init the tango env"
echo "For each step, press enter to do it or ctrl-c to cancel" 
echo "--------------------------------------"
echo ""
echo ""
echo "--- CVS login ---"
read -n 1 -s
echo ""
cvs login
echo ""
echo "done !"

echo ""
echo ""
echo ""

echo "--- get bin+command directories ---"
echo ""
read -n 1 -s
cd gen 
cvs -d :pserver:d24@jupiter.lusis:12401/lusis co -d bin TANGO/ptfTango/bin
cvs -d :pserver:d24@jupiter.lusis:12401/lusis co -d texp texp
cd - 1>/dev/null
cd test ; ln -s ~/gen/texp/command command ; cd - 1>/dev/null
echo ""
echo "done !"

echo ""
echo ""
echo ""

echo "--- prepare_env.pl sync ---"
echo ""
read -n 1 -s
cd gen ; prepare_env.pl sync ; cd - 1>/dev/null
echo ""
echo "done !"

echo ""
echo ""
echo ""

echo "--- comp.pl ---"
echo ""
read -n 1 -s
cd gen ; comp.pl ; cd - 1>/dev/null
echo ""
echo "done !"

echo ""
echo ""
echo ""

echo "--- getBin > config/modules.xml ---"
echo ""
read -n 1 -s
cd test ; getBin > config/modules.xml ; cd - 1>/dev/null
echo ""
echo "done !"

echo ""
echo ""
echo ""
